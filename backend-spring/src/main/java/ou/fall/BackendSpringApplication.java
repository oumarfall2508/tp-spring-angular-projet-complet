package ou.fall;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ou.fall.entities.Payment;
import ou.fall.entities.Student;
import ou.fall.enumerations.PaymentStatus;
import ou.fall.enumerations.PaymentType;
import ou.fall.repositories.StudentRepository;
import ou.fall.repositories.PaymentRepository;

import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;

@SpringBootApplication
public class BackendSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendSpringApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(StudentRepository studentRepository, PaymentRepository paymentRepository){
		return args -> {
            studentRepository.save(
					Student.builder()
							.id(UUID.randomUUID().toString())
							.firstName("Mohammed")
							.code("1121")
							.programId("PrgA")
							.build());
			studentRepository.save(
					Student.builder()
							.id(UUID.randomUUID().toString())
							.firstName("Fatim")
							.code("1122")
							.programId("PrgA")
							.build());
			studentRepository.save(
					Student.builder()
							.id(UUID.randomUUID().toString())
							.firstName("Aliou")
							.code("1123")
							.programId("PrgA")
							.build());
			studentRepository.save(
					Student.builder()
							.id(UUID.randomUUID().toString())
							.firstName("Saliou")
							.code("1124")
							.programId("PrgB")
							.build());

			PaymentType[] paymentType = PaymentType.values();
			Random random = new Random();
			studentRepository.findAll().forEach(student->{
				for (int i=0; i<10; i++){
					int indexPayment = random.nextInt(paymentType.length);
					Payment payment = Payment.builder()
							.student(student)
							.type(paymentType[indexPayment])
							.status(PaymentStatus.CREATED)
							.date(LocalDate.now())
							.amount(2000+(int) (Math.random()*300))
							.build();
					paymentRepository.save(payment);
				}
			});
		};
	}
}
