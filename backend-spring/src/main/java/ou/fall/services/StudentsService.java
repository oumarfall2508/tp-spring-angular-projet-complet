package ou.fall.services;

import ou.fall.entities.Student;

import java.util.List;

public interface StudentsService {
    public Student getStudentById(String id);

    public List<Student> getAllStudents();

    public Student getStudentByCode(String code);

    public List<Student> getStudentsByProgram(String idProgram);
}
