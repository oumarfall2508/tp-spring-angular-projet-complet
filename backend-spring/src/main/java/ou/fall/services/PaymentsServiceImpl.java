package ou.fall.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ou.fall.entities.Payment;
import ou.fall.entities.Student;
import ou.fall.enumerations.PaymentStatus;
import ou.fall.enumerations.PaymentType;
import ou.fall.repositories.PaymentRepository;
import ou.fall.repositories.StudentRepository;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class PaymentsServiceImpl implements PaymentsService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Payment getPaymentById(Long id) {
        return paymentRepository.findById(id).get();
    }

    @Override
    public Payment updatePaymentStatus(Long id, PaymentStatus status) {
        Payment payment = paymentRepository.findById(id).orElseThrow();
        payment.setStatus(status);
        return paymentRepository.save(payment);

    }

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    @Override
    public List<Payment> getPaymentByStudent(Student student) {
        return paymentRepository.findByStudent(student);
    }

    @Override
    public List<Payment> getPaymentByStudentCode(String code) {
        return paymentRepository.findByStudentCode(code);
    }

    @Override
    public Payment savePayment(MultipartFile file, LocalDate date, double amount, PaymentType type, String code) throws IOException {
        Path folderpath= Paths.get(System.getProperty("user.home"),"TP","Payments");
        if(Files.notExists(folderpath)){
            Files.createDirectories(folderpath);
        }
        String fileName= UUID.randomUUID().toString();
        Path filepath= Paths.get(System.getProperty("user.home"),"TP","Payments",fileName+".pdf");
       Files.copy(file.getInputStream(),filepath);

       Payment payment = Payment.builder()
               .amount(amount)
               .date(date)
               .type(type)
               .status(PaymentStatus.CREATED)
               .student(studentRepository.findByCode(code))
               .file(filepath.toUri().toString())
               .build();
      return paymentRepository.save(payment);
    }

    @Override
    public byte[] getPaymentFile(Long id) throws IOException {
        Payment payment = paymentRepository.findById(id).orElseThrow();
        String file = payment.getFile();
        return Files.readAllBytes(Path.of(URI.create(file)));
    }

    @Override
    public List<Payment> getPaymentBySatus(PaymentStatus status) {
        return paymentRepository.findByStatus(status);
    }

    @Override
    public List<Payment> getPaymentByType(PaymentType type) {
        return paymentRepository.findByType(type);
    }
}
