package ou.fall.services;

import org.springframework.web.multipart.MultipartFile;
import ou.fall.entities.Payment;
import ou.fall.entities.Student;
import ou.fall.enumerations.PaymentStatus;
import ou.fall.enumerations.PaymentType;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface PaymentsService {
    public Payment getPaymentById(Long id);

    public Payment updatePaymentStatus(Long id, PaymentStatus status);

    public List<Payment> getAllPayments();

    public List<Payment> getPaymentBySatus(PaymentStatus status);

    public List<Payment> getPaymentByType(PaymentType type);
    public List<Payment> getPaymentByStudent(Student student);
    public List<Payment> getPaymentByStudentCode(String code);

    public Payment savePayment(MultipartFile file, LocalDate date, double amount, PaymentType type, String code) throws IOException;

    public byte[] getPaymentFile(Long id) throws IOException;
}
