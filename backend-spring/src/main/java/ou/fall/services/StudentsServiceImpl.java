package ou.fall.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ou.fall.entities.Student;
import ou.fall.repositories.StudentRepository;

import java.util.List;

@Service
public class StudentsServiceImpl implements StudentsService {
    @Autowired
    private StudentRepository studentRepository;
    @Override
    public Student getStudentById(String id) {
        return studentRepository.findById(id).get();
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student getStudentByCode(String code) {
        return studentRepository.findByCode(code);
    }

    @Override
    public List<Student> getStudentsByProgram(String idProgram) {
        return studentRepository.findByProgramId(idProgram);
    }
}
