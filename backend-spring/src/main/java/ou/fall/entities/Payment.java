package ou.fall.entities;

import jakarta.persistence.*;
import lombok.*;
import ou.fall.enumerations.PaymentStatus;
import ou.fall.enumerations.PaymentType;

import java.time.LocalDate;

@Entity
@Getter @Setter @AllArgsConstructor @NoArgsConstructor @ToString @Builder
public class Payment {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate date;
    private double amount;
    private PaymentType type;
    private PaymentStatus status;
    private String file;
    @ManyToOne
    private Student student;
}
