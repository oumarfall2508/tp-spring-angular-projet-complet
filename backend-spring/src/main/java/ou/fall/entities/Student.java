package ou.fall.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Entity
@Getter @Setter @AllArgsConstructor @NoArgsConstructor @ToString @Builder
public class Student {
    @Id
    private String id;
    private String firstName;
    @Column(unique = true)
    private String code;
    private  String programId;
    private String photo;
}
