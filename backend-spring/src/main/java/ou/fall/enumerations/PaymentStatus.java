package ou.fall.enumerations;

public enum PaymentStatus {
    CREATED, VALIDATED, REJECTED
}
