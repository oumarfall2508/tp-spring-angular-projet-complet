package ou.fall.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ou.fall.entities.Student;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,String> {
    Student findByCode(String code);
    List<Student> findByProgramId(String programId);
}
