package ou.fall.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.multipart.MultipartFile;
import ou.fall.entities.Payment;
import ou.fall.entities.Student;
import ou.fall.enumerations.PaymentStatus;
import ou.fall.enumerations.PaymentType;

import java.time.LocalDate;
import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment,Long> {
    List<Payment> findByStudentCode(String code);
    List<Payment> findByStudent(Student student);
     List<Payment> findByStatus(PaymentStatus status);

    List<Payment> findByType(PaymentType type);

}
