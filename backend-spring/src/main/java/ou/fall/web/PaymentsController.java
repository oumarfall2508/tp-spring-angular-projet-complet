package ou.fall.web;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ou.fall.entities.Payment;
import ou.fall.entities.Student;
import ou.fall.enumerations.PaymentStatus;
import ou.fall.enumerations.PaymentType;
import ou.fall.services.PaymentsService;
import ou.fall.services.StudentsService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/payments")
@AllArgsConstructor
public class PaymentsController {
    private PaymentsService paymentsService;

    @GetMapping("/{id}")
    public Payment getPaymentById(@PathVariable(name = "id") Long id){
        return paymentsService.getPaymentById(id);
    }

    @GetMapping("/ByStatus")
    public List<Payment> getPaymentByStatus(@RequestParam PaymentStatus status){
        return paymentsService.getPaymentBySatus(status);
    }
    @GetMapping("/ByType")
    public List<Payment> getPaymentByType(@RequestParam PaymentType type){
        return paymentsService.getPaymentByType(type);
    }
    @GetMapping("")
    public List<Payment> getAllPayments(){
        return paymentsService.getAllPayments();
    }

    @PutMapping("/{id}/updateStatus")
    public Payment updatePaymentStatus(@PathVariable Long id, @RequestParam PaymentStatus status){
        return paymentsService.updatePaymentStatus(id, status);
    }

    @PostMapping(path = "", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Payment savePayment(@RequestBody MultipartFile file, LocalDate date, double amount, PaymentType type, String code) throws IOException {
        return paymentsService.savePayment(file,date,amount,type,code);
    }

    @GetMapping(path = "/{id}/PaymentFile", produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] getPaymentFile(@PathVariable Long id) throws IOException {
        return paymentsService.getPaymentFile(id);
    }
}
