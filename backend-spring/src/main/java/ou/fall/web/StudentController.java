package ou.fall.web;

import jakarta.websocket.server.PathParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ou.fall.entities.Payment;
import ou.fall.entities.Student;
import ou.fall.services.PaymentsService;
import ou.fall.services.StudentsService;

import java.util.List;

@RestController
@RequestMapping("/students")
@AllArgsConstructor
public class StudentController {
    private StudentsService studentsService;
    private PaymentsService paymentsService;

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable(name = "id") String id){
        return studentsService.getStudentById(id);
    }

    @GetMapping("/ByCode")
    public Student getStudentByCode(@RequestParam String code){
        return studentsService.getStudentByCode(code);
    }
    @GetMapping("")
    public List<Student> getAllStudent(){
        return studentsService.getAllStudents();
    }

    @GetMapping("/{id}/Payments")
    public List<Payment> getPaymentStudent(@PathVariable(name = "id") String id){
        Student student = studentsService.getStudentById(id);
         return  paymentsService.getPaymentByStudent(student);
    }
}
